package net.ethx.taoc.t2018

import net.ethx.taoc.FileDay
import net.ethx.taoc.util.{AGrid, Vec2}

import scala.collection.mutable

sealed trait Acre {
  val char: Char
}
case object Open extends Acre() {
  override val char: Char = '.'
}
case object Trees extends Acre {
  override val char: Char = '|'
}
case object Lumberyard extends Acre {
  override val char: Char = '#'
}

object Day18 extends FileDay[AGrid[Acre], Int, Int](2018, 18) {
  override def parse(lines: Seq[String]) = {
    val width = lines.head.length
    val height = lines.length
    val grid = new AGrid[Acre](width, height, null, stringify = _.char.toString)
    for {
      (line, y) <- lines.zipWithIndex
      (char, x) <- line.zipWithIndex
      a = char match {
        case Open.char => Open
        case Trees.char => Trees
        case Lumberyard.char => Lumberyard
      }
    } grid.set(x, y, a)
    grid
  }

  class Evolver(val target: Int) {
    lazy val seen : mutable.Map[String, (Int, AGrid[Acre])] = new mutable.HashMap[String, (Int, AGrid[Acre])]()

    def evolve(k: AGrid[Acre]) : AGrid[Acre] = {
      def _evolve(grid: AGrid[Acre], idx: Int): AGrid[Acre] = {
        def neighbours(c: Vec2) : Seq[Vec2] = {
          for {
            xd <- -1 to 1
            yd <- -1 to 1
            n = Vec2(xd, yd) + c
            if n != c && n.x >= 0 && n.x < grid.width && n.y >= 0 && n.y < grid.height
          } yield n
        }

        def evolve(c: Vec2, v: Acre) = {
          val ns = neighbours(c).map(grid.get)
          v match {
            case Open => if (ns.count(_ == Trees) >= 3) Trees else Open
            case Trees => if (ns.count(_ == Lumberyard) >= 3) Lumberyard else Trees
            case Lumberyard => if (ns.contains(Lumberyard) && ns.contains(Trees)) Lumberyard else Open
          }
        }

        val ret = AGrid(grid)
        grid.values().foreach(c => ret.set(c._1, evolve(c._1, c._2)))
        ret
      }

      var current = k
      for (x <- 1 to target) {
        current = _evolve(current, x)
        if (seen.contains(current.toString)) {
          val prior = seen(current.toString)
          val base = prior._1
          val mod = x - base
          return seen.values.find(_._1 == base + ((target - base) % mod)).get._2
        } else {
          seen.put(current.toString, (x, current))
        }
      }
      current
    }
  }

  override def part1(k: AGrid[Acre]) = {
    val terminal = new Evolver(10).evolve(k)
    val acres = terminal.coords().map(terminal.get)
    acres.count(_ == Trees) * acres.count(_ == Lumberyard)
  }

  override def part2(k: AGrid[Acre], t: Int) = {
    val terminal = new Evolver(1000000000).evolve(k)
    val acres = terminal.coords().map(terminal.get)
    acres.count(_ == Trees) * acres.count(_ == Lumberyard)
  }
}
