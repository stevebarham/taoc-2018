package net.ethx.taoc.t2018

import net.ethx.taoc.IterDay

object Day2 extends IterDay[String, Int, String](2018, 2) {
  override def parse(line: String) = line

  override def part1(lines: Seq[String]) = {
    def has(line: String, count: Int) : Boolean = line.toCharArray.groupBy(c => c).mapValues(_.length).values.exists(_ == count)

    lines.map(l => has(l, 2)).count(p => p) * lines.map(l => has(l, 3)).count(p => p)
  }

  override def part2(lines: Seq[String], part1: Int) = {
    def common(l1: String, l2: String) : String = Range(0, l1.length).filter(i => l1(i) == l2(i) ).map(l1(_)).mkString("")
    def diff(l1: String, l2: String) : Int = Range(0, l1.length).map(i => l1(i) != l2(i)).count(p => p)

    val forward = lines.sorted
    val backward = lines.sortBy(_.reverse)
    val pair = (forward ++ backward).sliding(2, 1).map(s => (s.head, s(1))).filter(p => diff(p._1, p._2) == 1).toSeq.head

    common(pair._1, pair._2)
  }
}
