package net.ethx.taoc.t2018

import net.ethx.taoc.IterDay
import net.ethx.taoc.util.Graph

import scala.collection.mutable

object Day7 extends IterDay[(String, String), String, Int](2018, 7) {
  override def parse(line: String) = {
    val pattern = "Step (.) must be finished before step (.) can begin.".r
    val pattern(from, to) = line
    (from, to)
  }

  def solve(cnt: Int, lines: Seq[(String, String)]) : (String, Int) = {
    val graph = new Graph[String]()
    lines.foreach(p => graph.add(p._1, p._2))

    val remaining = new mutable.HashSet[String]()
    graph.vertices().foreach(remaining.add)

    val complete = new mutable.LinkedHashSet[String]()
    class Worker(val id: Int) {
      var working : String = _
      var ticks : Int = 0

      def tick(): Unit = {
        if (ticks < 0) {
          val available = remaining.filter(v => graph.incomingVertices(v).diff(complete).isEmpty)
          if (available.nonEmpty) {
            val task = available.min

            remaining.remove(task)
            working = task
            ticks = 61 + (task.head - 'A')
          }
        }

        ticks = ticks - 1
        if (ticks == 0) {
          complete.add(working)
          working = null
        }
      }
    }

    val workers = Stream.from(0).map(new Worker(_)).take(cnt).toList
    println(s"Second\t${workers.map(w => "Worker " + (w.id + 1) + "\t").mkString}Done")

    var ticks = 0
    while (complete != graph.vertices()) {
      workers.foreach(_.tick())
      println(s"$ticks\t${workers.map(w => w.working + "\t").mkString}${complete.mkString}")

      ticks = ticks + 1
    }

    (complete.mkString(""), ticks)
  }

  override def part1(lines: Seq[(String, String)]) = solve(1, lines)._1

  override def part2(lines: Seq[(String, String)], part1: String) = solve(5, lines)._2
}
