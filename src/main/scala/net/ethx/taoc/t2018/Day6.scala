package net.ethx.taoc.t2018

import net.ethx.taoc.IterDay
import net.ethx.taoc.util.{AGrid, Vec2}

case class Label(label: Char, depth: Int)

class LGrid(width : Int, height: Int) extends AGrid[Label](width, height, null) {
  def contains(c: Vec2) : Boolean = c.x >= 0 && c.x < width && c.y >= 0 && c.y < height

  def flood(origin: Vec2, label: Char) : Unit = {
    set(origin, Label(label.toUpper, 0))
    for {
      cell <- coords()
      if origin != cell
      distance = (origin - cell).manhattan
      existing = get(cell)
    }  {
      if (existing == null) set(cell, Label(label, distance))
      else if (existing.depth == distance) set(cell, Label('.', distance))
      else if (existing.depth > distance) set(cell, Label(label, distance))
    }
  }

  override def toString: String = Range(0, height).map(y => Range(0, width).map(x => Option(get(x, y)).map(_.label.toString).getOrElse(".")).mkString("")).mkString("\n")
}

object Day6 extends IterDay[Vec2, Int, Int](2018, 6) {
  override def parse(line: String) = {
    val coords = line.split(',').map(_.trim.toInt)
    Vec2(coords.head, coords.last)
  }

  override def part1(lines: Seq[Vec2]) = {
    val grid = new LGrid(lines.map(_.x).max + 1, lines.map(_.y).max + 1)

    //  fill the grid
    val chars = Stream.iterate('a')(c => (c.toInt + 1).toChar)
    val charOrigins = chars.zip(lines).toMap
    charOrigins.foreach(t => grid.flood(t._2, t._1))

    //  count the grid
    val borderLabels = grid.border().map(grid.get(_).label.toLower).toSet
    val counts = grid.coords().map(grid.get(_).label.toLower).filter(l => !borderLabels.contains(l)).groupBy(c => c).mapValues(_.size)
    counts.maxBy(_._2)._2
  }

  override def part2(origins: Seq[Vec2], part1: Int) = {
    val grid = new LGrid(origins.map(_.x).max + 1, origins.map(_.y).max + 1)
    grid.coords().count(c => origins.map(o => (o - c).manhattan).sum < 10000)
  }
}
