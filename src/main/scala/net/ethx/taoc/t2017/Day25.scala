package net.ethx.taoc.t2017

import java.util

import net.ethx.taoc.Day

import scala.collection.JavaConverters._

case class Action(out: Int, move: Int, next: Char)
case class Turing(count: Int, initial: Char, table: Map[(Char, Int), Action])

object Day25 extends Day[Turing, Int, Int] {
  override def supply(): Turing = Turing(12134527, 'A', Map(
    ('A', 0) -> Action(1, 1, 'B'),
    ('A', 1) -> Action(0, -1, 'C'),
    ('B', 0) -> Action(1, -1, 'A'),
    ('B', 1) -> Action(1, 1, 'C'),
    ('C', 0) -> Action(1, 1, 'A'),
    ('C', 1) -> Action(0, -1, 'D'),
    ('D', 0) -> Action(1, -1, 'E'),
    ('D', 1) -> Action(1, -1, 'C'),
    ('E', 0) -> Action(1, 1, 'F'),
    ('E', 1) -> Action(1, 1, 'A'),
    ('F', 0) -> Action(1, 1, 'A'),
    ('F', 1) -> Action(1, 1, 'E')
  ))

  def run(t: Turing): Int = {
    val tape = new util.ArrayDeque[Int]()
    tape.addLast(0)

    var idx = 0
    var state = t.initial

    (0 until t.count).foreach(i => {
      val action = t.table((state, tape.pollFirst()))
      tape.addFirst(action.out)

      if (action.move == -1 && idx == 0)
        tape.addFirst(0)
      if (action.move == 1 && idx == tape.size() - 1)
        tape.addLast(0)

      if (action.move == -1)
        tape.addFirst(tape.pollLast())
      else
        tape.addLast(tape.pollFirst())

      state = action.next
    })

    tape.asScala.count(_ == 1)
  }

  override def part1(k: Turing) = run(k)

  override def part2(k: Turing, t: Int) = ???
}
