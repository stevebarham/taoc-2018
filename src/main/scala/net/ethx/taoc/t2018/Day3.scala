package net.ethx.taoc

import net.ethx.taoc.util.{IntGrid, Rec}

case class Claim(id: Int, rec: Rec)

object Day3 extends IterDay[Claim, Int, Int](2017, 3) {
  def parse(line: String) : Claim = {
    val bits = line.split(Array('#', ' ', '@', ',', ':', 'x')).filter(!_.isEmpty).map(_.toInt)
    Claim(bits(0), Rec(bits(1), bits(2), bits(3), bits(4)))
  }

  private def build(claims: Seq[Claim]) = {
    val rs = claims.map(_.rec)
    val maxx = rs.map(r => r.x + r.width).max
    val maxy = rs.map(r => r.y + r.height).max

    val rect = new IntGrid(maxx, maxy)
    rs.foreach(rect.fill)
    rect
  }

  override def part1(claims: Seq[Claim]) = {
    val rect = build(claims)
    rect.coords().count(c => rect.get(c) >= 2)
  }

  override def part2(claims: Seq[Claim], part1: Int): Int = {
    val rect = build(claims)
    claims.filter(c => rect.unique(c.rec)).toList.head.id
  }
}
