package net.ethx.taoc.t2018

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import net.ethx.taoc.IterDay

import scala.collection.mutable

sealed trait Event extends Comparable[Event] {
  val dttm: LocalDateTime
  override def compareTo(o: Event): Int = dttm.compareTo(o.dttm)
}
case class Change(dttm: LocalDateTime, id: Int) extends Event
case class Wake(dttm: LocalDateTime) extends Event
case class Sleep(dttm: LocalDateTime) extends Event

class Guard(val id: Int) {
  private val minutes = Array.fill(60)(0)
  private var slept : LocalDateTime = _

  def shuteye() : Int = minutes.sum

  def frequentistMinute() : Int = minutes.zipWithIndex.maxBy(_._1)._2

  def frequency() : Int = minutes.max

  def accept(event: Event): Unit = event match {
    case Change(_, _) | Wake(_) =>
      if (slept != null) Range.inclusive(slept.getMinute, event.dttm.getMinute).foreach(min => minutes(min) = minutes(min) + 1)
      slept = null
    case Sleep(dttm) => slept = dttm
  }
}

class Schedule {
  val guards = new mutable.HashMap[Int, Guard]()
  val timeToGuard = new java.util.TreeMap[LocalDateTime, Guard]()

  def guard(id: Int): Guard = guards.getOrElseUpdate(id, new Guard(id))

  def accept(event: Event) : Unit = {
    val current = timeToGuard.lowerEntry(event.dttm)
    event match {
      case Change(dttm, id) =>
        if (current != null) current.getValue.accept(event)
        timeToGuard.put(dttm, guard(id))
      case _ =>
        current.getValue.accept(event)
    }
  }
}

object Day4 extends IterDay[Event, Int, Int](2018, 4) {
  lazy val dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")

  override def parse(line: String) : Event = {
    val dttm = LocalDateTime.parse(line.substring(1, 17), dtf)
    if (line contains "Guard") Change(dttm, line.substring(line.indexOf('#') + 1, line.indexOf("begins") - 1).trim.toInt)
    else if (line.contains("asleep")) Sleep(dttm)
    else if (line.contains("wake")) Wake(dttm)
    else ???
  }

  override def part1(lines: Seq[Event]) : Int = {
    val sched = schedule(lines)
    val sleepiest = sched.guards.values.maxBy(_.shuteye())
    sleepiest.id * sleepiest.frequentistMinute()
  }

  override def part2(lines: Seq[Event], part1: Int) : Int = {
    val sched = schedule(lines)

    val guard = sched.guards.values.maxBy(g => g.frequency())
    guard.id * guard.frequentistMinute()
  }

  private def schedule(lines: Seq[Event]) : Schedule = {
    val schedule = new Schedule()
    lines.sorted.foreach(schedule.accept)
    schedule
  }
}
