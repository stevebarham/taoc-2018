package net.ethx.taoc

import scala.io.Source.fromResource

abstract class Day[S, T, V] extends App {
  def supply() : S
  def part1(k: S) : T
  def part2(k: S, t: T) : V

  val parsed = supply()
  val p1 = part1(parsed)
  println(s"part1 = $p1")

  val p2 = part2(parsed, p1)
  println(s"part2 = $p2")

  val cnt = 20
  val start = System.currentTimeMillis()
  Range.inclusive(1, cnt).foreach(i => part2(parsed, part1(parsed)))
  val now = System.currentTimeMillis()
  println(s"Took ${now - start}ms to run $cnt iterations (~ ${(now - start) / cnt}ms per op)")
}

abstract class FileDay[K, T, V](val year: Int, val day: Int) extends Day[K, T, V] {
  def load() : Seq[String] = fromResource(s"$year/day$day.txt").getLines().toSeq

  def parse(lines: Seq[String]) : K

  override def supply() = parse(load())
}

abstract class IterDay[K, T, V](year: Int, day: Int) extends FileDay[Seq[K], T, V](year, day) {
  def parse(line: String) : K

  override def parse(lines: Seq[String]) : Seq[K] = lines.map(parse)
}
