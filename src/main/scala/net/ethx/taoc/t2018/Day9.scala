package net.ethx.taoc.t2018

import net.ethx.taoc.IterDay

case class Spec(players: Int, marbles: Int)

object Day9 extends IterDay[Spec, Unit, Unit](2018, 9) {
  override def parse(line: String) = {
    val pattern = "(.+) players; last marble is worth (.+) points".r
    val pattern(ps, ms) = line
    Spec(ps.toInt, ms.toInt)
  }

  def simulate(spec: Spec) = {
    val scores = Array.fill(spec.players)(0.toLong)
    val circle = new java.util.ArrayDeque[Int]()
    circle.add(0)
    var currentIndex = 0

    def rotate(count: Int): Unit = {
      if (count > 0) Range(0, count).foreach(_ => circle.addFirst(circle.pollLast()))
      if (count < 0) Range(0, count.abs).foreach(_ => circle.addLast(circle.pollFirst()))
    }

    for (marble <- 1 to spec.marbles) {
      if (marble % 1000 == 0) print('.')
      if (marble % 100000 == 0) println(s". $marble")

      if (marble % 23 == 0) {
        rotate(7)
        val player = marble % scores.length
        scores(player) = scores(player) + marble + circle.pollLast()
        rotate(-1)
      } else {
        rotate(-1)
        circle.addLast(marble)
      }
    }

    scores.max
  }

  override def part1(lines: Seq[Spec]) = {
    lines.foreach(spec => println(s"\nhigh score is ${simulate(spec)}\n\n"))
  }

  override def part2(lines: Seq[Spec], part1: Unit) = this.part1(lines)
}
