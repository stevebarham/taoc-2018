package net.ethx.taoc.t2018

import java.util.concurrent.atomic.AtomicInteger

import net.ethx.taoc.IterDay
import net.ethx.taoc.util.{Vec2, IntGrid}

case class Result(c: Vec2, sum: Int, size: Int)

object Day11 extends IterDay[Int, Result, Result](2018, 11) {
  override def parse(line: String) = line.toInt

  class Calc(val serial: Int) {
    @inline def rackId(c: Vec2) = c.x + 10
    @inline def startingPower(c: Vec2) = rackId(c) * c.y
    @inline def hundreds(i: Int) = (i % 1000) / 100
    @inline def endPower(c: Vec2) = hundreds((startingPower(c) + serial) * rackId(c)) - 5
  }

  private def gridify(lines: Seq[Int]) = {
    val calc = new Calc(lines.head)
    val grid = new IntGrid(300, 300, 0, Vec2(1, 1))
    grid.coords().foreach(c => grid.set(c, calc.endPower(c)))
    grid
  }

  def search(grid: IntGrid, size: Int) : Result = {
    print(s"Searching $size... ")
    val recs = for {
      xs <- (1 to grid.width).sliding(size, 1)
      ys <- (1 to grid.height).sliding(size, 1)
      anchor = Vec2(xs.head, ys.head)
      sum = xs.flatMap(x => ys.map(y => grid.get(x, y))).sum
    } yield Result(anchor, sum, size)
    println(" done")
    recs.maxBy(_.sum)
  }

  override def part1(lines: Seq[Int]) = {
    require(new Calc(8).endPower(Vec2(3, 5)) == 4)
    require(new Calc(57).endPower(Vec2(122, 79)) == -5)
    require(new Calc(39).endPower(Vec2(217, 196)) == 0)
    require(new Calc(71).endPower(Vec2(101, 153)) == 4)
    search(gridify(lines), 3)
  }

  override def part2(lines: Seq[Int], part1: Result) = {
    val grid = gridify(lines)
    var cnt = new AtomicInteger()
    (1 to grid.width).par.map(size => {
      val ret = search(grid, size)
      println(s"Completed ${cnt.incrementAndGet()} searches")
      ret
    }).maxBy(_.sum)
  }
}
