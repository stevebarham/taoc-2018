package net.ethx.taoc.t2018

import net.ethx.taoc.FileDay

import scala.collection.mutable

object Day5 extends FileDay[String, Int, Int](2018, 5) {
  def react(in: String) : String = in.toCharArray.foldLeft(new mutable.StringBuilder())((sb, c) => {
    if (sb.nonEmpty && sb.last != c && sb.last.toLower == c.toLower) sb.deleteCharAt(sb.length - 1) else sb.append(c)
    sb
  }).toString()


  override def parse(lines: Seq[String]) = lines.mkString

  override def part1(line: String) = react(line).length

  override def part2(line: String, part1: Int) : Int = {
    def drop(in: String, drop: Char) = in.toCharArray.filter(c => c.toLower != drop.toLower).mkString
    line.toLowerCase.toSet.map((c : Char) => react(drop(line, c)).length).min
  }
}
