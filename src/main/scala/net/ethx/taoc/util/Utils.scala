package net.ethx.taoc.util

import java.lang.Math.abs

import scala.collection.mutable
import scala.reflect.ClassTag

case class Rec(x: Int, y: Int, width: Int, height: Int) {
  val right: Int = x + width
  val bottom: Int = y + height

  def intersects(that: Rec): Boolean = {
    this.x < that.right && this.right > that.x && this.y < that.bottom && this.bottom > that.y
  }

  def area() : Long = width.toLong * height.toLong
}

case class Vec2(x: Int, y: Int) {
  def manhattan: Int = abs(x)  + abs(y)

  def neighbours(): Seq[Vec2] = Seq(
    Vec2(x - 1, y),
    Vec2(x, y - 1),
    Vec2(x + 1, y),
    Vec2(x, y + 1)
  )

  def -(that: Vec2) : Vec2 = Vec2(this.x - that.x, this.y - that.y)

  def +(that: Vec2) : Vec2 = Vec2(this.x + that.x, this.y + that.y)

  def *(that: Vec2) : Vec2 = Vec2(this.x * that.x, this.y * that.y)

  def *(i: Int) : Vec2 = Vec2(this.x * i, this.y * i)
}

object AGrid {
  def apply[T: ClassTag](grid: AGrid[T]): AGrid[T] = {
    val ret = new AGrid(grid.width, grid.height, grid.default, grid.offset, grid.stringify)
    grid.arr.foreach(t => ret.arr.put(t._1, t._2))
    ret
  }
}

class AGrid[T: ClassTag](val width: Int, val height: Int, val default: T, val offset: Vec2 = Vec2(0, 0), val stringify: T => String = (t: T) => t.toString) {
  private val arr: mutable.Map[Vec2, T] = new mutable.HashMap[Vec2, T]()

  def get(c: Vec2) : T = arr.getOrElse(c - offset, default)
  def get(x: Int, y: Int) : T = get(Vec2(x, y))

  def set(c: Vec2, t: T) : T = arr.put(c - offset, t).getOrElse(default)
  def set(x: Int, y: Int, t: T) : T = set(Vec2(x, y), t)

  def coords() : Seq[Vec2] = for {
    x <- Range(0, width)
    y <- Range(0, height)
  } yield Vec2(x, y) + offset

  def border() : Seq[Vec2] = {
    val right = width - 1
    val bottom = height - 1
    coords().filter(c => (c.x == 0 || c.x == right) || (c.y == 0 || c.y == bottom)).map(_ + offset)
  }

  def values(): Seq[(Vec2, T)] = arr.toSeq

  override def toString: String = {
    val sb = new mutable.StringBuilder()
    for (y <- Range(0, height)) {
      for (x <- Range(0, width)) {
        sb.append(stringify(get(x, y)))
      }
      sb.append("\n")
    }
    sb.toString()
  }
}

class IntGrid(width: Int, height: Int, default: Int = 0, offset: Vec2 = Vec2(0, 0)) extends AGrid[Int](width, height, default, offset) {
  def add(x: Int, y: Int): Unit = {
    val c = Vec2(x, y)
    set(c, get(c) + 1)
  }

  def fill(r: Rec): Unit = Range.apply(r.x, r.x + r.width).foreach(x => Range.apply(r.y, r.y + r.height).foreach(y => add(x, y)))

  def unique(r: Rec): Boolean = Range.apply(r.x, r.x + r.width).forall(x => {
    Range.apply(r.y, r.y + r.height).forall(y => 1 == get(Vec2(x, y)))
  })
}

class CharGrid(width: Int, height: Int) extends AGrid[Char](width, height, ' ')

class Graph[V] {
  private val outgoing: mutable.Map[V, mutable.Set[V]] = new mutable.HashMap[V, mutable.Set[V]]()
  private val incoming: mutable.Map[V, mutable.Set[V]] = new mutable.HashMap[V, mutable.Set[V]]()

  def vertices() : collection.Set[V] = outgoing.keySet

  def add(vertex: V) : Unit = {
    Seq(incoming, outgoing).foreach(m => if (!m.contains(vertex)) m(vertex) = new mutable.HashSet[V]())
  }

  def add(source: V, target: V) : Unit = {
    add(source)
    add(target)
    outgoing(source).add(target)
    incoming(target).add(source)
  }

  def outgoingVertices(vertex: V): mutable.Set[V] = outgoing(vertex)

  def incomingVertices(vertex: V): mutable.Set[V] = incoming(vertex)
}

