package net.ethx.taoc.t2018

import net.ethx.taoc.IterDay

import scala.annotation.tailrec
import scala.collection.mutable

object Day12 extends IterDay[(String, Char), Long, Long](2018, 12) {
  override def parse(line: String): (String, Char) = if (line.startsWith("initial")) (line.split(':')(1).trim, ' ') else (line.take(5), line.last)

  type State = mutable.TreeMap[Int, Char]
  def format(s: State) = (s.firstKey to s.lastKey).map(s(_)).mkString
  def sum(s: State): Int = s.map(t => if (t._2 == '#') t._1 else 0).sum

  private def run(k: Seq[(String, Char)], target: Long): Long = {
    //  build our lookup
    val lookup = k.tail.toMap

    //  define our evolution function
    def evolve(current: State): State = {
      def slice(i: Int) = (i - 2 to i + 2).map(current.getOrElse(_, '.')).mkString
      val ret = (current.firstKey - 5 to current.lastKey + 5).foldLeft(new State())((next, i) => next.updated(i, lookup.getOrElse(slice(i), '.')).asInstanceOf[State])
      while (ret(ret.firstKey) == '.') ret.remove(ret.firstKey)
      while (ret(ret.lastKey) == '.') ret.remove(ret.lastKey)
      ret
    }

    //  define our solver function
    @tailrec def solve(target: Long, idx: Long, s: State) : Long = {
      val next = evolve(s)
      if (target == idx) {
        sum(next)
      } else if (format(next) == format(s)) {
        val diff = sum(next) - sum(s)
        sum(next) + (target - idx) * diff
      } else {
        solve(target, idx + 1, next)
      }
    }

    //  build the initial state and solve
    val state = k.head._1.zipWithIndex.foldLeft(new State())((s, t) => s.updated(t._2, t._1).asInstanceOf[State])
    solve(target, 1, state)
  }


  override def part1(k: Seq[(String, Char)]): Long = run(k, 20)

  override def part2(k: Seq[(String, Char)], t: Long): Long = run(k, 50000000000L)
}
