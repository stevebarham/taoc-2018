package net.ethx.taoc.t2017

import net.ethx.taoc.IterDay

import scala.annotation.tailrec

case class Reg(id: Char)

sealed trait Expr
case class RegExpr(reg: Reg) extends Expr
case class ConsExpr(cons: Int) extends Expr

sealed trait Op
case class Set(reg: Reg, y: Expr) extends Op
case class Sub(reg: Reg, y: Expr) extends Op
case class Mul(reg: Reg, y: Expr) extends Op
case class Jnz(x: Expr, y: Expr) extends Op

case class State(cpu: Map[Reg, Int], log: List[Op], ops: List[Op], pc: Int) {
  def eval(e: Expr): Int = e match {
    case ConsExpr(c) => c
    case RegExpr(r) => cpu(r)
  }

  @tailrec final def run(): State = {
    if (pc == ops.length) {
      this
    } else {
      val op = ops(pc)

      println("--")
      println(cpu)
      println(op)
      println("--")

      val next = op match {
        case Set(r, y) => copy(cpu.updated(r, eval(y)), List(op) ++ log, ops, pc + 1)
        case Sub(r, y) => copy(cpu.updated(r, cpu(r) - eval(y)), List(op) ++ log, ops, pc + 1)
        case Mul(r, y) => copy(cpu.updated(r, cpu(r) * eval(y)), List(op) ++ log, ops, pc + 1)
        case Jnz(x, y) => copy(cpu, List(op) ++ log, ops, pc + (if (eval(x) != 0) eval(y) else 1))
      }

      next.run()
    }
  }
}

object Day23 extends IterDay[Op, Int, Int](2017, 23) {
  def reg(token: String) = Reg(token.head)
  def expr(token: String) : Expr = if (token.length == 1 && Character.isAlphabetic(token.head)) RegExpr(reg(token)) else ConsExpr(token.toInt)

  override def parse(line: String): Op = line.split(' ').toList match {
    case "set" :: x :: y :: Nil => Set(reg(x), expr(y))
    case "sub" :: x :: y :: Nil => Sub(reg(x), expr(y))
    case "mul" :: x :: y :: Nil => Mul(reg(x), expr(y))
    case "jnz" :: x :: y :: Nil => Jnz(expr(x), expr(y))
    case _ => ???
  }

  override def part1(lines: Seq[Op]) = {
    val state = State(Seq('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h').map(c => Reg(c) -> 0).toMap, List(), lines.toList, 0)
    state.run().log.count(_.isInstanceOf[Mul])
  }

  override def part2(lines: Seq[Op], part1: Int) = {
    val state = State(Seq('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h').map(c => Reg(c) -> 0).toMap.updated(Reg('a'), 1), List(), lines.toList, 0)
    state.run().log.count(_.isInstanceOf[Mul])
  }
}
