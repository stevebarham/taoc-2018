package net.ethx.taoc.t2018

import net.ethx.taoc.IterDay
import net.ethx.taoc.util.{CharGrid, Vec2, Rec}

object Day10 extends IterDay[(Vec2, (Int, Int)), String, String](2018, 10) {
  override def parse(line: String) : (Vec2, (Int, Int)) = {
    val ss = line.split(Array('=', ',', '<', '>')).map(_.trim)
    (Vec2(ss(2).toInt, ss(3).toInt), (ss(6).toInt, ss(7).toInt))
  }

  case class Points(cs: Seq[Vec2]) {
    private lazy val xs = cs.map(_.x)
    private lazy val ys = cs.map(_.y)

    lazy val minX: Int = xs.min
    lazy val maxX: Int = xs.max
    lazy val minY: Int = ys.min
    lazy val maxY: Int = ys.max
  }

  override def part1(lines: Seq[(Vec2, (Int, Int))]) = {
    def rec(ps: Points) = Rec(ps.minX, ps.minY, ps.maxX - ps.minX, ps.maxY - ps.minY)
    def cost(ps: Points) : Long = rec(ps).area()

    def evolve(i: Int) : Points = Points(lines.map(t => {
      val (coord, vector) = (t._1, t._2)
      Vec2(coord.x + vector._1 * i, coord.y + vector._2 * i)
    }))

    //  find the optimal arrangement (~ smallest area)
    val stream = Stream.from(0).map(evolve)
    val minimum = stream.zip(stream.tail).zipWithIndex.dropWhile(p => cost(p._1._1) >= cost(p._1._2)).head

    //  render
    def grid(ps: Points) = {
      val r = rec(ps)
      def tx(c: Vec2) = Vec2(c.x - r.x, c.y - r.y)

      val grid = new CharGrid(r.width + 1, r.height + 1)
      ps.cs.foreach(c => grid.set(tx(c), 'X'))
      grid
    }

    "\n at iteration " + minimum._2 + "\n" + grid(minimum._1._1).toString
  }

  override def part2(lines: Seq[(Vec2, (Int, Int))], part1: String) = ""
}
