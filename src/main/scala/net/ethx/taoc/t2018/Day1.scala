package net.ethx.taoc.t2018

import fs2.Stream.emits
import net.ethx.taoc.IterDay

import scala.collection.mutable

object Day1 extends IterDay[Int, Int, Int](2018, 1) {
  override def parse(line: String) = line.toInt

  override def part1(lines: Seq[Int]) : Int = lines.sum

  override def part2(nums: Seq[Int], part1: Int): Int = {
    val seen = mutable.HashSet[Int]()
    emits(nums).repeat.scan1(_ + _).find(!seen.add(_)).toList.head
  }
}
