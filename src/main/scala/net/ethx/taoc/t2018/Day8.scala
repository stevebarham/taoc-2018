package net.ethx.taoc.t2018

import net.ethx.taoc.FileDay

import scala.collection.Iterator.{empty, single}
import scala.collection.mutable

case class Node(children: List[Node], meta: Seq[Int]) extends mutable.Iterable[Node] {
  val value : Int = if (children.isEmpty) {
    meta.sum
  } else {
    meta.map(idx => if (idx <= children.length) children(idx - 1).value else 0).sum
  }

  override def iterator: Iterator[Node] = single(this) ++ children.map(_.iterator).fold(empty)(_ ++ _)
}

object Day8 extends FileDay[Node, Int, Int](2018, 8) {
  def parse(tokens: mutable.Queue[Int]) : Node = {
    val childCount = tokens.dequeue()
    val metaCount = tokens.dequeue()
    Node(
      children = (0 until childCount).map(_ => parse(tokens)).toList,
      meta = (0 until metaCount).map(_ => tokens.dequeue()).toList
    )
  }

  override def parse(line: Seq[String]) : Node = {
    val q = new mutable.Queue[Int]()
    q.enqueue(line.mkString.split(' ').map(s => s.trim.toInt):_*)
    parse(q)
  }

  override def part1(node: Node) = node.flatMap(n => n.meta).sum

  override def part2(node: Node, part1: Int) = node.value
}
